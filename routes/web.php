<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DayController@home');
Route::get('home/{date?}', 'DayController@home');

Route::get('day/{measure}/{date?}', 'DayController@index');

Route::get('history/{measure}/{state}', 'HistoryController@index');

Route::get('state/{state}/{date?}', 'StateController@index');

Route::get('us/history/{measure}', 'UsHistoryController@index');

Route::get('us/{date?}', 'UsController@index');

Route::get('definitions', 'MiscController@definitions');

Route::get('logs', 'MiscController@logsSummary');
Route::get('logs_detail', 'MiscController@logsDetail');
