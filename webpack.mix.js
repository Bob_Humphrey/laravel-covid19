const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/alpine.js', 'public/js/all.js')
.styles('resources/css/tailwind.css', 'public/css/all.css')
.copyDirectory('resources/img', 'public/img')
.copy('resources/favicon.ico', 'public/favicon.ico')
.copy('resources/fonts/*.*', 'public/css');
