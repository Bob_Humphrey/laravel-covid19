@extends('layouts.app')

@section('content')

<x-pageTitle :pageTitle="$pageTitle" :date="$date" pageType="day" :path="$path" />
<x-dateMenu :path="$path" />
<x-map :stateData="$stateData" />
<x-scaleDisplay :scale="$scale" />
<x-dayDataTable :stateData="$stateData" :usData="$usData" :path="$path" :date="$date" :measureName="$pageTitle" />

@endsection