@php
use Illuminate\Support\Facades\Log;
//Log::info($usData);
@endphp

@extends('layouts.app')

@section('content')

  <x-simplePageTitle pageTitle="US Overview" :date="$date" pageType="home" />
  <x-dateMenu :path="$path" />
  <x-usTotals :usData="$usData" />

  <x-historyDataChart :data="$sevenDayAvgCasesData" measure="seven_day_cases_average" :date="$date" />

  <div class="text-xl text-center font-inter_semibold text-gray-600">Seven Day Cases Daily Average</div>

  <div class="pb-12">
    <x-improvementDataChart :dataWorse="$casesWorse" :dataSame="$casesSame" :dataBetter="$casesBetter" :date="$date" />
  </div>

  <x-map :stateData="$stateDataCasesPerCapita" />
  <x-scaleDisplay :scale="$scaleCasesPerCapita" />

  <div class="text-xl text-center font-inter_semibold text-gray-600 pt-4 pb-24">Seven Day Average Cases Per Million</div>

  <div class="text-xl text-center font-inter_semibold text-gray-600">Fourteen Day Cases Pct Change</div>

  <x-dayDataTable :stateData="$stateData" :usData="$usTableData" :path="$pathTable" :date="$date"
    measureName="Fourteen Day Cases Trend" />

  </div>

@endsection
