@extends('layouts.app')

@section('content')

  <x-simplePageTitle :pageTitle="$pageTitle" :date="$date" pageType="state" />
  <x-dateMenu :path="$path" />

  <div class="w-full">
    <x-historyDataChart :data="$sevenDayCasesData" :date="$date" />
    <div class="text-base text-center font-inter_medium text-gray-600">Seven Day Cases Daily Average</div>
  </div>

  <x-stateDataTable :record="$record" :date="$date" :state="$state" :stateName="$pageTitle" :population="$population" />

@endsection
