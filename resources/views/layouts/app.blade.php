<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Scripts -->
  <script src={{ asset('js/all.js') }} />

  <!-- Matomo -->
  <script type="text/javascript">
    var _paq = window._paq || [];
      /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      (function() {
        var u="//analytics.bob-humphrey.com/";
        _paq.push(['setTrackerUrl', u+'matomo.php']);
        _paq.push(['setSiteId', '4']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
      })();
  </script>
  <!-- End Matomo Code -->

  <!-- Styles -->
  <link rel="stylesheet" href="{{ asset('css/all.css') }}">
  <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" />

</head>

<body>
  <div>

    <main class="lg:flex justify-center w-full">
      <div class="lg:grid grid-cols-3 gap-16 w-full lg:w-5/6 xl:w-2/3 pt-12 pb-24 px-8 md:px-16 lg:px-0">
        <div class="col-span-1">
          <div class="hidden lg:block">
            <div class="pb-4">
              <x-title />
            </div>
            <x-menu />
            <x-credits />
            <x-articles />
            <x-author />
          </div>
          <div class="lg:hidden" x-data="{ showMenu: false }">
            <div class="flex justify-between leading-none pb-4">
              <div class=" flex justify-start">
                <x-title />
              </div>
              <div class="flex justify-end cursor-pointer items-center w-8 " @click=" showMenu=!showMenu"
                x-show="showMenu">
                @svg('close', 'fill-current')
              </div>
              <div class="flex justify-end cursor-pointer items-center w-8 " @click=" showMenu=!showMenu"
                x-show="!showMenu">
                @svg('menu', 'fill-current')
              </div>
            </div>
            <div x-show="showMenu">
              <x-menu />
              <x-credits />
              <x-author />
            </div>
          </div>
        </div>
        <div class="col-span-2">
          @yield('content')
        </div>
      </div>
    </main>
  </div>

</body>

</html>