<div class="text-base md:text-xl font-inter_semibold text-gray-600 border-b border-gray-400 pt-8">
  <table class="w-full">

    {{-- Population --}}

    <tr class="grid grid-cols-12 py-1 border-t border-gray-400">
      <td></td>
      <td class="hidden md:block col-span-1 text-right py-2 px-3">
        {{ $population->population_rank}}
      </td>
      <td class="col-span-7 md:col-span-6 py-2 pl-1">
        Population
      </td>
      <td class="col-span-3 py-2 px-2 text-right">
        {{ $population->formattedPopulation }}
      </td>
      <td></td>
    </tr>

    {{-- State statistics --}}

    @foreach ($dataTable as $row)
    <tr class="grid grid-cols-12 py-1 border-t border-gray-400">
      <td style={{ $row->style }}>
      </td>
      <td class="hidden md:block col-span-1 text-right py-2 px-3">
        {{ $row->rank}}
      </td>
      <td class="md:hidden col-span-6 hover:text-red-600 py-2 pl-1 tooltip">
        <a href={{ $row->url }}>
          <span class="tooltip-text">
            {{ $row->name }} - {{ $formattedDate }}
          </span>
          {{ substr($row->name, 0, 18) }}
        </a>
      </td>
      <td class="hidden md:block col-span-6 hover:text-red-600 py-2 pl-1 tooltip">
        <a href={{ $row->url }}>
          <span class="tooltip-text">
            {{ $row->name }} - {{ $formattedDate }}
          </span>
          {{ $row->name }}
        </a>
      </td>
      <td class="col-span-4 md:col-span-3 py-2 px-2 text-right">
        {{ $row->formattedMeasure }}
      </td>
      <td
        class="flex justify-end self-center col-span-1 hover:text-red-600 hover:cursor-pointer py-2 text-right tooltip">
        <a href={{ $row->historyUrl }}>
          <div class="w-6">
            @svg('calendar', 'fill-current')
          </div>
          <span class="tooltip-text">
            {{ $row->name }} - {{ $stateName }}
          </span>
        </a>
      </td>
    </tr>
    @endforeach
  </table>
</div>