<footer class="flex w-full justify-center bg-gray-200 py-32 mt-16">
  <div class="w-16">
    <a href="https://bob-humphrey.com">
      <img src="{{ asset('img/bh-logo.gif') }}" alt="Logo" />
    </a>
  </div>
</footer>