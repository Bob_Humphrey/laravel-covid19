<div class="text-base md:text-xl font-inter_semibold text-gray-600 border-b border-gray-400 pt-8">
  <table class="w-full">

    {{-- National Data  --}}

    <tr class="grid grid-cols-12 py-1 border-t border-gray-400">
      <td></td>
      <td>
      </td>
      <td class="col-span-5 hover:text-red-600 py-2 pl-1 tooltip">
        <a href={{ $usUrl }}>
          <span class="tooltip-text">
            United States - {{ $formattedDate }}
          </span>
          United States
        </a>
      </td>
      <td class="col-span-4 py-2 px-2 text-right">
        {{ $usData->formattedMeasure }}
      </td>
      <td
        class="flex justify-end self-center col-span-1 hover:text-red-600 hover:cursor-pointer py-2 text-right tooltip">
        <a href={{ $usHistoryUrl }}>
          <span class="tooltip-text">
            {{ $measureName }} - United States
          </span>
          <div class="w-6">
            @svg('calendar', 'fill-current')
          </div>
        </a>
      </td>
    </tr>

    {{-- State Data --}}


    @foreach ($dataTable as $row)
    <tr class="grid grid-cols-12 py-1 border-t border-gray-400">
      <td style={{ $row->style }}></td>
      <td class="col-span-1 text-right py-2 px-1 sm:px-3">
        {{ $row->rank}}
      </td>
      <td class="col-span-5 hover:text-red-600 py-2 pl-1 tooltip">
        <a href={{ $row->stateUrl }}>
          <span class="tooltip-text">
            {{ $row->name }}- {{ $formattedDate }}
          </span>
          <span class="sm:hidden">
            {{ $row->nameShort }}
          </span>
          <span class="hidden sm:block">
            {{ $row->name }}
          </span>
        </a>
      </td>
      <td class="col-span-4 py-2 px-2 text-right">
        {{ $row->formattedMeasure }}
      </td>
      <td
        class="flex justify-end self-center col-span-1 hover:text-red-600 hover:cursor-pointer py-2 text-right tooltip">
        <a href={{ $row->historyUrl }}>
          <div class="w-6">
            @svg('calendar', 'fill-current')
          </div>
          <span class="tooltip-text">
            {{ $measureName }} - {{ $row->name }}
          </span>
        </a>
      </td>
    </tr>
    @endforeach
  </table>
</div>