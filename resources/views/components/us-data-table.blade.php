<div class="text-base md:text-xl font-inter_semibold text-gray-600 border-b border-gray-400 pt-8">
  <table class="w-full">

    {{-- Population --}}

    <tr class="grid grid-cols-12 py-1 border-t border-gray-400">
      <td class="col-span-8 py-2 pl-1">
        Population
      </td>
      <td class="col-span-3 py-2 text-right">
        {{ $population->formattedPopulation }}
      </td>
      <td></td>
    </tr>

    {{-- State statistics --}}

    @foreach ($dataTable as $row)
    <tr class="grid grid-cols-12 py-1 border-t border-gray-400">
      <td class="col-span-7 hover:text-red-600 py-2 pl-1 tooltip">
        <a href={{ $row->url }}>
          <span class="tooltip-text">
            {{ $row->name }} - {{ $formattedDate }}
          </span>
          <span class="sm:hidden">
            {{ $row->nameShort }}
          </span>
          <span class="hidden sm:block">
            {{ $row->name }}
          </span>
        </a>
      </td>
      <td class="col-span-4 py-2 px-2 text-right">
        {{ $row->formattedMeasure }}
      </td>
      <td
        class="flex justify-end self-center col-span-1 hover:text-red-600 hover:cursor-pointer py-2 text-right tooltip">
        <a href={{ $row->historyUrl }}>
          <div class="w-6">
            @svg('calendar', 'fill-current')
          </div>
          <span class="tooltip-text">
            {{ $row->name }} - United States
          </span>
        </a>
      </td>
    </tr>
    @endforeach
  </table>
</div>