<div class="flex w-full py-8">
  <div class="w-16">
    <a href="https://bob-humphrey.com">
      <img src="{{ asset('img/bh-logo-grey.gif') }}" alt="Logo" />
    </a>
  </div>
</div>