<div class="flex w-full font-inter_light">
  <a class="date-button hidden xl:block" href={{  $earliestDateUrl }}>
    {{ $earliestDateDisplay }}
  </a>
  <a class="date-button hidden md:block" href={{  $weekBackDateUrl }}>
    {{ $weekBackDateDisplay }}
  </a>
  <a class="date-button" href={{  $dayBackDateUrl }}>
    {{ $dayBackDateDisplay }}
  </a>
  <a class="date-button" href={{  $dayForwardDateUrl }}>
    {{ $dayForwardDateDisplay }}
  </a>
  <a class="date-button hidden md:block" href={{  $weekForwardDateUrl }}>
    {{ $weekForwardDateDisplay }}
  </a>
  <a class="date-button hidden xl:block" href={{  $latestDateUrl }}>
    {{ $latestDateDisplay }}
  </a>
</div>