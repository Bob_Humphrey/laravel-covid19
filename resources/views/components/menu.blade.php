@php
use App\Helpers\Measures;
use App\Helpers\States;
$stateCount = count(States::$states);
$columns = 4;
$statesPerColumn = ceil($stateCount / $columns);
@endphp

<div class="text-lg font-inter_medium text-gray-600 pb-4">

  <ul class="pb-4">
    <li class="flex hover:text-red-600">
      <a href={{ url('definitions') }}>
        Definitions
      </a>
      <div class="flex cursor-pointer items-center w-6 ml-2">
        @svg('information-outline', 'fill-current')
      </div>
    </li>
  </ul>

  {{-- DEATHS --}}

  <h3 class="text-gray-800 text-2xl font-inter_bold">Deaths</h3>
  <ul class="">
    <li class="hover:text-red-600"><a href={{ url('day/total_deaths') }}>{{ Measures::getHeading('total_deaths') }}</a>
    </li>
    <li class="hover:text-red-600"><a
        href={{ url('day/total_deaths_per_capita') }}>{{ Measures::getHeading('total_deaths_per_capita') }}</a></li>
    {{-- <li class="hover:text-red-600"><a
        href={{ url('day/seven_day_deaths') }}>{{ Measures::getHeading('seven_day_deaths') }}</a></li>
    --}}
    <li class="hover:text-red-600"><a
        href={{ url('day/seven_day_deaths_average') }}>{{ Measures::getHeading('seven_day_deaths_average') }}</a></li>
    {{-- <li class="hover:text-red-600"><a
        href={{ url('day/seven_day_deaths_increase') }}>{{ Measures::getHeading('seven_day_deaths_increase') }}</a></li>
    --}}
    <li class="hover:text-red-600"><a
        href={{ url('day/fourteen_day_deaths_change') }}>{{ Measures::getHeading('fourteen_day_deaths_change') }}</a>
    </li>
    {{-- <li class="hover:text-red-600"><a
        href={{ url('day/doubling_deaths') }}>{{ Measures::getHeading('doubling_deaths') }}</a>
    </li> --}}
    <li class="hover:text-red-600"><a href={{ url('day/daily_deaths') }}>{{ Measures::getHeading('daily_deaths') }}</a>
    </li>
  </ul>

  {{-- CASES --}}

  <h3 class="text-gray-800 text-2xl font-inter_bold pt-4">Cases</h3>
  <ul class="">
    <li class="hover:text-red-600"><a href={{ url('day/total_cases') }}>{{ Measures::getHeading('total_cases') }}</a>
    </li>
    <li class="hover:text-red-600"><a
        href={{ url('day/total_cases_per_capita') }}>{{ Measures::getHeading('total_cases_per_capita') }}</a></li>
    {{-- <li class="hover:text-red-600"><a
        href={{ url('day/seven_day_cases') }}>{{ Measures::getHeading('seven_day_cases') }}</a>
    </li> --}}
    <li class="hover:text-red-600"><a
        href={{ url('day/seven_day_cases_average') }}>{{ Measures::getHeading('seven_day_cases_average') }}</a></li>
    <li class="hover:text-red-600"><a
        href={{ url('day/average_cases_per_capita') }}>{{ Measures::getHeading('average_cases_per_capita') }}</a>
    </li>
    {{-- <li class="hover:text-red-600"><a
        href={{ url('day/seven_day_cases_increase') }}>{{ Measures::getHeading('seven_day_cases_increase') }}</a></li>
    --}}
    <li class="hover:text-red-600"><a
        href={{ url('day/fourteen_day_cases_change') }}>{{ Measures::getHeading('fourteen_day_cases_change') }}</a></li>
    {{-- <li class="hover:text-red-600"><a
        href={{ url('day/doubling_cases') }}>{{ Measures::getHeading('doubling_cases') }}</a>
    </li> --}}
    <li class="hover:text-red-600"><a href={{ url('day/daily_cases') }}>{{ Measures::getHeading('daily_cases') }}</a>
    </li>
  </ul>

  {{-- TESTS --}}

  <h3 class="text-gray-800 text-2xl font-inter_bold pt-4">Tests</h3>
  <ul class="">
    <li class="hover:text-red-600"><a href={{ url('day/total_tests') }}>{{ Measures::getHeading('total_tests') }}</a>
    </li>
    {{-- <li class="hover:text-red-600"><a
        href={{ url('day/total_tests_per_capita') }}>{{ Measures::getHeading('total_tests_per_capita') }}</a></li>
    <li class="hover:text-red-600"><a
        href={{ url('day/seven_day_tests') }}>{{ Measures::getHeading('seven_day_tests') }}</a>
    </li> --}}
    <li class="hover:text-red-600"><a
        href={{ url('day/seven_day_tests_average') }}>{{ Measures::getHeading('seven_day_tests_average') }}</a></li>
    {{-- <li class="hover:text-red-600"><a
        href={{ url('day/seven_day_tests_increase') }}>{{ Measures::getHeading('seven_day_tests_increase') }}</a></li>
    <li class="hover:text-red-600"><a
        href={{ url('day/fourteen_day_tests_change') }}>{{ Measures::getHeading('fourteen_day_tests_change') }}</a></li>
    <li class="hover:text-red-600"><a
        href={{ url('day/doubling_tests') }}>{{ Measures::getHeading('doubling_tests') }}</a>
    </li> --}}
    {{-- <li class="hover:text-red-600"><a
        href={{ url('day/daily_tests') }}>{{ Measures::getHeading('daily_tests') }}</a>
    </li> --}}
    <li class="hover:text-red-600"><a
        href={{ url('day/tests_percent_positive') }}>{{ Measures::getHeading('tests_percent_positive') }}</a></li>
  </ul>

  {{-- NATION --}}

  <h3 class="text-gray-800 text-2xl font-inter_bold pt-4">Nation</h3>
  <div class="hover:text-red-600">
    <a href={{ url('us/') }}>US</a>
  </div>


  {{-- STATES --}}

  <h3 class="text-gray-800 text-2xl font-inter_bold pt-4">States</h3>
  <div class="flex">
    @for ($column = 0; $column < $columns; $column++)
      <div class="w-full lg:w-1/4 px-1">

        @php
        $firstCategory = $column * $statesPerColumn;
        $lastCategory = ($column + 1) * $statesPerColumn;
        @endphp

        @foreach (States::$states as $key => $value)

          @if ($loop->index >= $firstCategory && $loop->index < $lastCategory)
            <div class="hover:text-red-600"><a href={{ url('state/' . $key) }}> {{ $key }}</a></div>
          @endif

        @endforeach

      </div>
    @endfor
  </div>

</div>
