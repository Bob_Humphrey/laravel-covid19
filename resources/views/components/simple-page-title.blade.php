<div class="font-inter_bold border-b border-gray-400 pb-4 mb-6">
  <div class="text-3xl text-gray-800">
    {{ $pageTitle }}
  </div>
  <div class="text-2xl  text-gray-600">
    {{ $pageSubtitle }}
  </div>
</div>