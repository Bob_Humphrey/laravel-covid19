<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateDailyUsMeasures extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('us_historical_data', function (Blueprint $table) {
      $table->renameColumn('seven_day_deaths_change', 'fourteen_day_deaths_change');
      $table->renameColumn('category_seven_day_deaths_change', 'category_fourteen_day_deaths_change');
      $table->renameColumn('rank_seven_day_deaths_change', 'rank_fourteen_day_deaths_change');
      $table->renameColumn('seven_day_cases_change', 'fourteen_day_cases_change');
      $table->renameColumn('category_seven_day_cases_change', 'category_fourteen_day_cases_change');
      $table->renameColumn('rank_seven_day_cases_change', 'rank_fourteen_day_cases_change');
      $table->renameColumn('seven_day_tests_change', 'fourteen_day_tests_change');
      $table->renameColumn('category_seven_day_tests_change', 'category_fourteen_day_tests_change');
      $table->renameColumn('rank_seven_day_tests_change', 'rank_fourteen_day_tests_change');
      $table->decimal('tests_percent_positive', 4, 1)->default(0.0)->index();
      $table->tinyInteger('category_tests_percent_positive')->default(0);
      $table->tinyInteger('rank_tests_percent_positive')->default(0)->index();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    //
  }
}
