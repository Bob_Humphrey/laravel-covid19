<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStateTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('state', function (Blueprint $table) {
      $table->id();
      $table->string('abbreviation', 2)->index();
      $table->string('name', 50);
      $table->integer('population');
      $table->tinyInteger('population_rank');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('state');
  }
}
