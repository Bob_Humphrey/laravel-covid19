<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDailyUsMeasures extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('us_historical_data', function (Blueprint $table) {
      $table->id();
      $table->date('day')->index();
      $table->string('state', 2)->index();

      // DEATHS

      $table->integer('total_deaths')->default(0);
      $table->tinyInteger('category_total_deaths')->default(0);
      $table->tinyInteger('rank_total_deaths')->default(0)->index();

      $table->decimal('total_deaths_per_capita', 8, 1)->default(0.0);
      $table->tinyInteger('category_total_deaths_per_capita')->default(0);
      $table->tinyInteger('rank_total_deaths_per_capita')->default(0)->index();

      $table->integer('seven_day_deaths')->default(0);
      $table->tinyInteger('category_seven_day_deaths')->default(0);
      $table->tinyInteger('rank_seven_day_deaths')->default(0)->index();

      $table->decimal('seven_day_deaths_increase', 8, 2)->default(0.0);
      $table->tinyInteger('category_seven_day_deaths_increase')->default(0);
      $table->tinyInteger('rank_seven_day_deaths_increase')->default(0)->index();

      $table->decimal('seven_day_deaths_average', 9, 2)->default(0.0);
      $table->tinyInteger('category_seven_day_deaths_average')->default(0);
      $table->tinyInteger('rank_seven_day_deaths_average')->default(0)->index();

      $table->decimal('seven_day_deaths_change', 9, 2)->default(0.0);
      $table->tinyInteger('category_seven_day_deaths_change')->default(0);
      $table->tinyInteger('rank_seven_day_deaths_change')->default(0)->index();

      $table->integer('doubling_deaths')->default(0);
      $table->tinyInteger('category_doubling_deaths')->default(0);
      $table->tinyInteger('rank_doubling_deaths')->default(0)->index();

      $table->integer('daily_deaths')->default(0);
      $table->tinyInteger('category_daily_deaths')->default(0);
      $table->tinyInteger('rank_daily_deaths')->default(0)->index();

      // CASES

      $table->integer('total_cases')->default(0);
      $table->tinyInteger('category_total_cases')->default(0);
      $table->tinyInteger('rank_total_cases')->default(0)->index();

      $table->decimal('total_cases_per_capita', 9, 1)->default(0.0);
      $table->tinyInteger('category_total_cases_per_capita')->default(0);
      $table->tinyInteger('rank_total_cases_per_capita')->default(0)->index();

      $table->integer('seven_day_cases')->default(0);
      $table->tinyInteger('category_seven_day_cases')->default(0);
      $table->tinyInteger('rank_seven_day_cases')->default(0)->index();

      $table->decimal('seven_day_cases_increase', 9, 2)->default(0.0);
      $table->tinyInteger('category_seven_day_cases_increase')->default(0);
      $table->tinyInteger('rank_seven_day_cases_increase')->default(0)->index();

      $table->decimal('seven_day_cases_average', 10, 2)->default(0.0);
      $table->tinyInteger('category_seven_day_cases_average')->default(0);
      $table->tinyInteger('rank_seven_day_cases_average')->default(0)->index();

      $table->decimal('seven_day_cases_change', 10, 2)->default(0.0);
      $table->tinyInteger('category_seven_day_cases_change')->default(0);
      $table->tinyInteger('rank_seven_day_cases_change')->default(0)->index();

      $table->integer('doubling_cases')->default(0);
      $table->tinyInteger('category_doubling_cases')->default(0);
      $table->tinyInteger('rank_doubling_cases')->default(0)->index();

      $table->integer('daily_cases')->default(0);
      $table->tinyInteger('category_daily_cases')->default(0);
      $table->tinyInteger('rank_daily_cases')->default(0)->index();

      // TESTS

      $table->integer('total_tests')->default(0);
      $table->tinyInteger('category_total_tests')->default(0);
      $table->tinyInteger('rank_total_tests')->default(0)->index();

      $table->decimal('total_tests_per_capita', 9, 1)->default(0.0);
      $table->tinyInteger('category_total_tests_per_capita')->default(0);
      $table->tinyInteger('rank_total_tests_per_capita')->default(0)->index();

      $table->integer('seven_day_tests')->default(0);
      $table->tinyInteger('category_seven_day_tests')->default(0);
      $table->tinyInteger('rank_seven_day_tests')->default(0)->index();

      $table->decimal('seven_day_tests_increase', 9, 2)->default(0.0);
      $table->tinyInteger('category_seven_day_tests_increase')->default(0);
      $table->tinyInteger('rank_seven_day_tests_increase')->default(0)->index();

      $table->decimal('seven_day_tests_average', 10, 2)->default(0.0);
      $table->tinyInteger('category_seven_day_tests_average')->default(0);
      $table->tinyInteger('rank_seven_day_tests_average')->default(0)->index();

      $table->decimal('seven_day_tests_change', 10, 2)->default(0.0);
      $table->tinyInteger('category_seven_day_tests_change')->default(0);
      $table->tinyInteger('rank_seven_day_tests_change')->default(0)->index();

      $table->integer('doubling_tests')->default(0);
      $table->tinyInteger('category_doubling_tests')->default(0);
      $table->tinyInteger('rank_doubling_tests')->default(0)->index();

      $table->integer('daily_tests')->default(0);
      $table->tinyInteger('category_daily_tests')->default(0);
      $table->tinyInteger('rank_daily_tests')->default(0)->index();

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('us_historical_data');
  }
}
