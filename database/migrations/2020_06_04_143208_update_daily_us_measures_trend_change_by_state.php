<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateDailyUsMeasuresTrendChangeByState extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('us_historical_data', function (Blueprint $table) {
      $table->tinyInteger('fourteen_day_deaths_better')->default(0);
      $table->tinyInteger('fourteen_day_deaths_same')->default(0);
      $table->tinyInteger('fourteen_day_deaths_worse')->default(0);
      $table->tinyInteger('fourteen_day_cases_better')->default(0);
      $table->tinyInteger('fourteen_day_cases_same')->default(0);
      $table->tinyInteger('fourteen_day_cases_worse')->default(0);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    //
  }
}
