<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\LoadStateTableJob;

class LoadStateTable extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'app:load_state_table';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Load state table from excel spreadsheet';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    LoadStateTableJob::dispatch();
  }
}
