<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\UpdateRankingsJob;

class UpdateRankingsDeaths extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'app:update_rankings_deaths';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Update rankings - deaths';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    UpdateRankingsJob::dispatch('total_deaths');
    UpdateRankingsJob::dispatch('total_deaths_per_capita');
    UpdateRankingsJob::dispatch('seven_day_deaths');
    UpdateRankingsJob::dispatch('seven_day_deaths_increase');
    UpdateRankingsJob::dispatch('seven_day_deaths_average');
    UpdateRankingsJob::dispatch('fourteen_day_deaths_change');
    UpdateRankingsJob::dispatch('doubling_deaths');
    UpdateRankingsJob::dispatch('daily_deaths');
  }
}
