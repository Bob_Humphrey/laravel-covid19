<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\UpdateRankingsJob;

class UpdateRankingsCasesTemp extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'app:update_rankings_cases_temp';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Update rankings - cases';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    UpdateRankingsJob::dispatch('average_cases_per_capita');
  }
}
