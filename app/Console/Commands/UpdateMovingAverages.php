<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Command\UpdateMovingAveragesJob;
use App\Jobs\UpdateMovingAveragesJob as JobsUpdateMovingAveragesJob;

class UpdateMovingAverages extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'app:update_moving_averages';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Update history moving average fields';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    JobsUpdateMovingAveragesJob::dispatch();
  }
}
