<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\UpdateRankingsJob;

class UpdateRankingsCases extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'app:update_rankings_cases';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Update rankings - cases';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    UpdateRankingsJob::dispatch('total_cases');
    UpdateRankingsJob::dispatch('total_cases_per_capita');
    UpdateRankingsJob::dispatch('seven_day_cases');
    UpdateRankingsJob::dispatch('seven_day_cases_increase');
    UpdateRankingsJob::dispatch('seven_day_cases_average');
    UpdateRankingsJob::dispatch('fourteen_day_cases_change');
    UpdateRankingsJob::dispatch('doubling_cases');
    UpdateRankingsJob::dispatch('daily_cases');
    UpdateRankingsJob::dispatch('average_cases_per_capita');
  }
}
