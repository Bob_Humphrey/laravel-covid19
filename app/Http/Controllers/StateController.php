<?php

namespace App\Http\Controllers;

use App\StateHistoricalData;
use App\StateData;
use App\Helpers\States;
use Carbon\Carbon;
use stdClass;

class StateController extends Controller
{
  public function index($state, $date = null)
  {
    if ($date === null) {
      $date = session('last_used_date');
    }
    if ($date === null) {
      $date = Carbon::now()->subDay()->format('Y-m-d');
    }
    session(['last_used_date' => $date]);
    $stateName = States::getStateName($state);

    // Data charts

    $sevenDayDeathsData = [];
    $fourteenDayDeathsData = [];
    $sevenDayCasesData = [];
    $fourteenDayCasesData = [];

    $d = StateHistoricalData::select(
      'day',
      'seven_day_deaths_average',
      'fourteen_day_deaths_change',
      'seven_day_cases_average',
      'fourteen_day_cases_change'
    )
      ->where('state', $state)
      ->orderBy('day', 'DESC')
      ->get();

    foreach ($d as $o) {
      $item = new stdClass();
      $item->day = $o->day;
      $item->measure = $o->seven_day_deaths_average;
      $sevenDayDeathsData[] = $item;
      $item = new stdClass();
      $item->day = $o->day;
      $item->measure = $o->fourteen_day_deaths_change;
      $fourteenDayDeathsData[] = $item;
      $item = new stdClass();
      $item->day = $o->day;
      $item->measure = $o->seven_day_cases_average;
      $sevenDayCasesData[] = $item;
      $item = new stdClass();
      $item->day = $o->day;
      $item->measure = $o->fourteen_day_cases_change;
      $fourteenDayCasesData[] = $item;
    }

    // Population

    $record = StateHistoricalData::where('state', $state)
      ->where('day', $date)
      ->first();

    $population = StateData::where('abbreviation', $state)->first();

    return view('state', [
      'state' => $state,
      'pageTitle' => $stateName,
      'record' => $record,
      'date' => $date,
      'sevenDayDeathsData' => $sevenDayDeathsData,
      'fourteenDayDeathsData' => $fourteenDayDeathsData,
      'sevenDayCasesData' => $sevenDayCasesData,
      'fourteenDayCasesData' => $fourteenDayCasesData,
      'population' => $population,
      'path' => 'state/' . $state
    ]);
  }
}
