<?php

namespace App\Http\Controllers;

use App\StateHistoricalData;
use App\Helpers\Scales;
use App\Helpers\States;
use App\Helpers\Measures;
use stdClass;

class HistoryController extends Controller
{
  public function index($measure, $state)
  {
    $stateName = States::getStateName($state);
    $category = 'category_' . $measure;
    $rank = 'rank_' . $measure;
    $scaleProperty = $measure . '_scale';
    $objScales = new Scales($scaleProperty);
    $scale = $objScales->getScale();
    $pageTitle = Measures::getHeading($measure);
    $data = [];

    $d = StateHistoricalData::select('day', $measure, $category, $rank)
      ->where('state', $state)
      ->orderBy('day', 'DESC')
      ->get();

    foreach ($d as $o) {
      $item = new stdClass();
      $item->day = $o->day;
      $item->measure = $o[$measure];
      $item->category = $o[$category];
      $item->rank = $o[$rank];
      $data[] = $item;
    }

    return view('history', [
      'pageTitle' => $pageTitle,
      'state' => $state,
      'stateName' => $stateName,
      'measure' => $measure,
      'data' => $data,
      'scale' => $scale
    ]);
  }
}
