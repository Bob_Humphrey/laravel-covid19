<?php

namespace App\Http\Controllers;

use App\USHistoricalData;
use App\Helpers\Measures;
use stdClass;

class UsHistoryController extends Controller
{
  public function index($measure)
  {
    $pageTitle = Measures::getHeading($measure);
    $data = [];

    $d = USHistoricalData::select('day', $measure)
      ->orderBy('day', 'DESC')
      ->get();

    foreach ($d as $o) {
      $item = new stdClass();
      $item->day = $o->day;
      $item->measure = $o[$measure];
      $item->category = 0;
      $item->rank = 0;
      $data[] = $item;
    }

    return view('us-history', [
      'pageTitle' => $pageTitle,
      'measure' => $measure,
      'data' => $data,
    ]);
  }
}
