<?php

namespace App\Http\Controllers;

use App\JobLog;
use App\Helpers\Measures;

class MiscController extends Controller
{
  public function logsSummary()
  {
    $data = JobLog::where('job_name', 'Director')
      ->orderBy('id', 'DESC')
      ->limit(1000)
      ->get();

    return view('logs', [
      'data' => $data,
      'report' => 'summary'
    ]);
  }

  public function logsDetail()
  {
    $data = JobLog::orderBy('id', 'DESC')->limit(1000)->get();

    return view('logs', [
      'data' => $data,
      'report' => 'detail'
    ]);
  }

  public function definitions()
  {
    $measures = Measures::$measures;
    $definitions = Measures::$definitions;

    return view('definitions', [
      'measures' => $measures,
      'definitions' => $definitions,
    ]);
  }
}
