<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\StateHistoricalData;
use App\USHistoricalData;
use App\Helpers\Misc;

class UpdateMovingAveragesJob implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {
    $job = Misc::jobStart('UpdateMovingAveragesJob');

    StateHistoricalData::chunk(200, function ($records) use ($job) {
      foreach ($records as $r) {

        $carbonDate = Carbon::createFromFormat('Y-m-d', $r->day);
        if ($carbonDate->lessThan($job['earliestDate'])) {
          continue;
        }
        if ($carbonDate->greaterThan($job['latestDate'])) {
          continue;
        }

        $twoWeeksAgo = Misc::getTwoWeeksAgo($r->day);
        $state = $r->state;

        $r14 = StateHistoricalData::where('state', $state)->where('day', $twoWeeksAgo)->first();
        if ($r14) {
          if ($r14->seven_day_deaths_average == 0) {
            $r->fourteen_day_deaths_change = 0;
          } else {
            $r->fourteen_day_deaths_change =
              (($r->seven_day_deaths_average - $r14->seven_day_deaths_average) / $r14->seven_day_deaths_average) * 100;
          }

          if ($r14->seven_day_cases_average == 0) {
            $r->fourteen_day_cases_change = 0;
          } else {
            $r->fourteen_day_cases_change =
              (($r->seven_day_cases_average - $r14->seven_day_cases_average) / $r14->seven_day_cases_average) * 100;
          }

          if ($r14->seven_day_tests_average == 0) {
            $r->fourteen_day_tests_change = 0;
          } else {
            $r->fourteen_day_tests_change =
              (($r->seven_day_tests_average - $r14->seven_day_tests_average) / $r14->seven_day_tests_average) * 100;
          }
        }

        if ($r->seven_day_tests_average == 0) {
          $r->tests_percent_positive = 0;
        } else {
          $r->tests_percent_positive = ($r->seven_day_cases_average * 100) / $r->seven_day_tests_average;
        }

        $r->save();
      }
    });

    USHistoricalData::chunk(200, function ($records) use ($job) {
      foreach ($records as $r) {

        $carbonDate = Carbon::createFromFormat('Y-m-d', $r->day);
        if ($carbonDate->lessThan($job['earliestDate'])) {
          continue;
        }
        if ($carbonDate->greaterThan($job['latestDate'])) {
          continue;
        }

        $twoWeeksAgo = Misc::getTwoWeeksAgo($r->day);

        $r14 = USHistoricalData::where('day', $twoWeeksAgo)->first();
        if ($r14) {
          if ($r14->seven_day_deaths_average == 0) {
            $r->fourteen_day_deaths_change = 0;
          } else {
            $r->fourteen_day_deaths_change =
              (($r->seven_day_deaths_average - $r14->seven_day_deaths_average) / $r14->seven_day_deaths_average) * 100;
          }

          if ($r14->seven_day_cases_average == 0) {
            $r->fourteen_day_cases_change = 0;
          } else {
            $r->fourteen_day_cases_change =
              (($r->seven_day_cases_average - $r14->seven_day_cases_average) / $r14->seven_day_cases_average) * 100;
          }

          if ($r14->seven_day_tests_average == 0) {
            $r->fourteen_day_tests_change = 0;
          } else {
            $r->fourteen_day_tests_change =
              (($r->seven_day_tests_average - $r14->seven_day_tests_average) / $r14->seven_day_tests_average) * 100;
          }
        }

        if ($r->seven_day_tests_average == 0) {
          $r->tests_percent_positive = 0;
        } else {
          $r->tests_percent_positive = ($r->seven_day_cases_average * 100) / $r->seven_day_tests_average;
        }

        $r->save();
      }
    });

    Misc::jobEnd('UpdateMovingAveragesJob');
  }
}
