<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\StateHistoricalData;


class QuickTestJob implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {
    // Retrieve data from dev api
    $query_string = 'https://covidtracking.com/api/states/daily';
    $response = Http::get($query_string)->getBody();
    $apiRecords = json_decode((string) $response);

    for ($i = 0; $i < 10; $i++) {
      $r = $apiRecords[$i];
      $d = new StateHistoricalData();
      $strDate =  strval($r->date);
      $d->day = substr($strDate, 0, 4)
        . '-' . substr($strDate, 4, 2) . '-' . substr($strDate, 6, 2);
      $d->state = $r->state;
      $d->total_deaths = $r->death;
      $d->daily_deaths = $r->deathIncrease;
      $d->average_deaths = 0;
      $d->average_deaths_change = 0;
      $d->cumulative_tests = $r->totalTestResults;
      $d->daily_tests = $r->totalTestResultsIncrease;
      $d->average_tests = 0;
      $d->average_tests_change = 0;
      $d->cumulative_cases = $r->positive;
      $d->daily_cases = $r->positiveIncrease;
      $d->average_cases = 0;
      $d->average_cases_change = 0;
      $d->save();
    }
  }
}
