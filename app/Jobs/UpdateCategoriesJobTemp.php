<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use App\StateHistoricalData;
use App\Helpers\Scales;
use App\Helpers\Measures;
use App\Helpers\Misc;
use Carbon\Carbon;

class UpdateCategoriesJobTemp implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {
    $job = Misc::jobStart('UpdateCategoriesJobTemp');

    $measures = Measures::$measures;

    StateHistoricalData::chunk(50, function ($records) use ($measures, $job) {
      foreach ($records as $r) {

        $carbonDate = Carbon::createFromFormat('Y-m-d', $r->day);
        if ($carbonDate->lessThan($job['earliestDate'])) {
          continue;
        }
        if ($carbonDate->greaterThan($job['latestDate'])) {
          continue;
        }

        $key = 'average_cases_per_capita';
        $scale = $key . "_scale";
        $category = "category_" . $key;
        $objScales = new Scales($scale);
        $r[$category] = $objScales->getCategory($r[$key]);


        $r->save();
      }
    });

    Misc::jobEnd('UpdateCategoriesJobTemp');
  }
}
