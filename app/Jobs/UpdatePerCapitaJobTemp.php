<?php

namespace App\Jobs;

use App\StateData;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use App\Helpers\Us;
use App\Helpers\Misc;
use App\StateHistoricalData;
use App\USHistoricalData;
use Carbon\Carbon;

class UpdatePerCapitaJobTemp implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {
    $job = Misc::jobStart('UpdatePerCapitaJobTemp');

    // $stateData = StateData::get();
    // $statePopulation = array();
    // foreach ($stateData as $state) {
    //   $statePopulation[$state->abbreviation] = $state->population;
    // }

    // StateHistoricalData::chunk(200, function ($records) use ($statePopulation, $job) {
    //   foreach ($records as $r) {
    //     $carbonDate = Carbon::createFromFormat('Y-m-d', $r->day);
    //     if ($carbonDate->lessThan($job['earliestDate'])) {
    //       continue;
    //     }
    //     if ($carbonDate->greaterThan($job['latestDate'])) {
    //       continue;
    //     }
    //     $population = $statePopulation[$r->state];
    //     $r->average_cases_per_capita = $r->seven_day_cases_average * 1000000 / $population;
    //     $r->save();
    //   }
    // });

    $usPopulation = Us::$population;

    USHistoricalData::chunk(200, function ($records) use ($usPopulation, $job) {
      foreach ($records as $r) {
        $carbonDate = Carbon::createFromFormat('Y-m-d', $r->day);
        if ($carbonDate->lessThan($job['earliestDate'])) {
          continue;
        }
        if ($carbonDate->greaterThan($job['latestDate'])) {
          continue;
        }
        $r->average_cases_per_capita = $r->seven_day_cases_average * 1000000 / $usPopulation;
        $r->save();
      }
    });

    Misc::jobEnd('UpdatePerCapitaJobTemp');
  }
}
