<?php

namespace App\Helpers;

use Carbon\Carbon;
use App\JobDate;
use App\JobLog;

class Misc
{

  public static function jobStart($name)
  {
    $jobStart = Carbon::now();
    $updatedAt = $jobStart->toDateTimeString();

    $earliestDate = JobDate::where('type', 'earliest date')->first();
    $latestDate = JobDate::where('type', 'latest date')->first();
    $fEarliestDate = substr($earliestDate->date, 0, 4)
      . '-' . substr($earliestDate->date, 5, 2) . '-' . substr($earliestDate->date, 8, 2);
    $fLatestDate = substr($latestDate->date, 0, 4)
      . '-' . substr($latestDate->date, 5, 2) . '-' . substr($latestDate->date, 8, 2);
    $cEarliestDate = Carbon::createFromFormat('Y-m-d', $fEarliestDate);
    $cLatestDate = Carbon::createFromFormat('Y-m-d', $fLatestDate);

    $jobLog = new JobLog();
    $jobLog->job_name = $name;
    $jobLog->status = 'start';
    $jobLog->earliest_date = $fEarliestDate;
    $jobLog->latest_date = $fLatestDate;
    $jobLog->created_at = $updatedAt;
    $jobLog->updated_at = $updatedAt;
    $jobLog->save();

    return [
      'updatedAt' => $updatedAt,           // current date and time for database 
      'earliestDate' => $cEarliestDate,    // earliest date - Carbon format
      'latestDate' => $cLatestDate         // latest date - Carbon format
    ];
  }

  public static function jobEnd($name, $minutes = null)
  {
    $jobStart = Carbon::now();
    $updatedAt = $jobStart->toDateTimeString();

    $earliestDate = JobDate::where('type', 'earliest date')->first();
    $latestDate = JobDate::where('type', 'latest date')->first();
    $fEarliestDate = substr($earliestDate->date, 0, 4)
      . '-' . substr($earliestDate->date, 5, 2) . '-' . substr($earliestDate->date, 8, 2);
    $fLatestDate = substr($latestDate->date, 0, 4)
      . '-' . substr($latestDate->date, 5, 2) . '-' . substr($latestDate->date, 8, 2);

    $jobLog = new JobLog();
    $jobLog->job_name = $name;
    $jobLog->status = 'end';
    $jobLog->earliest_date = $fEarliestDate;
    $jobLog->latest_date = $fLatestDate;
    $jobLog->created_at = $updatedAt;
    $jobLog->updated_at = $updatedAt;
    if ($minutes) {
      $jobLog->run_minutes = $minutes;
    }
    $jobLog->save();
  }

  public static function formatDate($date)
  {
    $strDate =  strval($date);
    $formDate = substr($strDate, 0, 4)
      . '-' . substr($strDate, 4, 2) . '-' . substr($strDate, 6, 2);
    $carbonDate = Carbon::createFromFormat('Y-m-d', $formDate);
    return [
      'carbonDate' => $carbonDate,
      'formattedDate' => $formDate
    ];
  }

  public static function getWeekAgo($date)
  {
    $day = Carbon::createFromFormat('Y-m-d', $date);
    $dayMinus7 = $day->subWeek();
    $dayMinus7Formatted = $dayMinus7->toDateString();
    return $dayMinus7Formatted;
  }

  public static function getTwoWeeksAgo($date)
  {
    $day = Carbon::createFromFormat('Y-m-d', $date);
    $dayMinus14 = $day->subDays(14);
    $dayMinus14Formatted = $dayMinus14->toDateString();
    return $dayMinus14Formatted;
  }
}
