<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\Support\Facades\Log;
use App\Helpers\Scales;
use stdClass;

class Map extends Component
{
  public $stateData;
  public $mapData;
  /**
   * Create a new component instance.
   *
   * @return void
   */
  public function __construct($stateData)
  {
    $mapData =  new stdClass();
    $scales = new Scales();
    for ($i = 0; $i < count($stateData); $i++) {
      $s = $stateData[$i];
      $mapData->{$s->state} = $scales->getColor($s->category);
    }

    $this->mapData = $mapData;
  }

  /**
   * Get the view / contents that represent the component.
   *
   * @return \Illuminate\View\View|string
   */
  public function render()
  {
    return view('components.map');
  }
}
