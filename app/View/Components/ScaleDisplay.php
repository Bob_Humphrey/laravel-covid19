<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\Support\Facades\Log;

class ScaleDisplay extends Component
{
  public $scale;
  /**
   * Create a new component instance.
   *
   * @return void
   */
  public function __construct($scale)
  {
    $displayScale = [];
    for ($i = 0; $i < count($scale); $i++) {
      $item = strval($scale[$i]);
      $length = strlen($item);
      if ($length <= 3) {
        $displayScale[] = $item;
      } else {
        $last3Chars = substr($item, $length - 3, 3);
        if ($last3Chars == '000') {
          $firstChars = substr($item, 0, $length - 3);
          $displayScale[] = $firstChars . 'K';
        } else {
          $displayScale[] = $item;
        }
      }
    }
    $this->scale = $displayScale;
  }

  /**
   * Get the view / contents that represent the component.
   *
   * @return \Illuminate\View\View|string
   */
  public function render()
  {
    return view('components.scale');
  }
}
