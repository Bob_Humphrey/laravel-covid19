<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class DateMenu extends Component
{
  public $path;

  public $weekBackDateDisplay;
  public $dayBackDateDisplay;
  public $weekForwardDateDisplay;
  public $dayForwardDateDisplay;
  public $earliestDateDisplay;
  public $latestDateDisplay;

  public $weekBackDateYYYYMMDD;
  public $dayBackDateYYYYMMDD;
  public $weekForwardDateYYYYMMDD;
  public $dayForwardDateYYYYMMDD;
  public $earliestDateYYYYMMDD;
  public $latestDateYYYYMMDD;

  public $earliestDateUrl;
  public $weekBackDateUrl;
  public $dayBackDateUrl;
  public $dayForwardDateUrl;
  public $weekForwardDateUrl;
  public $latestDateUrl;
  /**
   * Create a new component instance.
   *
   * @return void
   */
  public function __construct($path)
  {
    $this->path = $path;
    $this->backButtonsActive = true;
    $this->forwardButtonsActive = true;
    $earliestDate = Carbon::createFromFormat('Y-m-d', session('earliest_date'));
    $latestDate = Carbon::yesterday();
    $lastUsedDate = Carbon::createFromFormat('Y-m-d', session('last_used_date'));
    $weekBackDate = Carbon::createFromFormat('Y-m-d', $lastUsedDate->format('Y-m-d'));
    $weekBackDate->subDays(7);
    $dayBackDate = Carbon::createFromFormat('Y-m-d', $lastUsedDate->format('Y-m-d'));
    $dayBackDate->subDays(1);
    $weekForwardDate = Carbon::createFromFormat('Y-m-d', $lastUsedDate->format('Y-m-d'));
    $weekForwardDate->addDays(7);
    $dayForwardDate = Carbon::createFromFormat('Y-m-d', $lastUsedDate->format('Y-m-d'));
    $dayForwardDate->addDays(1);

    if ($weekBackDate->lessThanOrEqualTo($earliestDate)) {
      $weekBackDate = $earliestDate;
    }

    if ($dayBackDate->lessThanOrEqualTo($earliestDate)) {
      $dayBackDate = $earliestDate;
    }

    if ($lastUsedDate->lessThanOrEqualTo($earliestDate)) {
      $this->backButtonsActive = false;
    }

    if ($weekForwardDate->greaterThanOrEqualTo($latestDate)) {
      $weekForwardDate = $latestDate;
    }

    if ($dayForwardDate->greaterThanOrEqualTo($latestDate)) {
      $dayForwardDate = $latestDate;
    }

    if ($lastUsedDate->greaterThanOrEqualTo($latestDate)) {
      $this->forwardButtonsActive = false;
    }

    $this->weekBackDateDisplay = 'Wk back ' . $weekBackDate->format('n/j');
    $this->weekBackDateYYYYMMDD = $weekBackDate->format('Y-m-d');

    $this->dayBackDateDisplay = 'Day back ' . $dayBackDate->format('n/j');
    $this->dayBackDateYYYYMMDD = $dayBackDate->format('Y-m-d');

    $this->weekForwardDateDisplay = 'Wk forward ' . $weekForwardDate->format('n/j');
    $this->weekForwardDateYYYYMMDD = $weekForwardDate->format('Y-m-d');

    $this->dayForwardDateDisplay = 'Day forward ' . $dayForwardDate->format('n/j');
    $this->dayForwardDateYYYYMMDD = $dayForwardDate->format('Y-m-d');

    $this->earliestDateDisplay = 'Start ' . $earliestDate->format('n/j');
    $this->earliestDateYYYYMMDD = $earliestDate->format('Y-m-d');

    $this->latestDateDisplay = 'End ' . $latestDate->format('n/j');
    $this->latestDateYYYYMMDD = $latestDate->format('Y-m-d');

    $this->earliestDateUrl = url($path . "/" . $this->earliestDateYYYYMMDD);
    $this->weekBackDateUrl = url($path . "/" . $this->weekBackDateYYYYMMDD);
    $this->dayBackDateUrl = url($path . "/" . $this->dayBackDateYYYYMMDD);
    $this->dayForwardDateUrl = url($path . "/" . $this->dayForwardDateYYYYMMDD);
    $this->weekForwardDateUrl = url($path . "/" . $this->weekForwardDateYYYYMMDD);
    $this->latestDateUrl = url($path . "/" . $this->latestDateYYYYMMDD);
  }

  /**
   * Get the view / contents that represent the component.
   *
   * @return \Illuminate\View\View|string
   */
  public function render()
  {
    return view('components.date-menu');
  }
}
