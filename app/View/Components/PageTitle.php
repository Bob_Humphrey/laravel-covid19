<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\Support\Facades\Log;
use App\Helpers\Measures;
use Carbon\Carbon;

class PageTitle extends Component
{
  public $pageTitle;
  public $pageSubtitle;
  public $pageType;
  public $date;
  public $path;
  public $definition;
  /**
   * Create a new component instance.
   *
   * @return void
   */
  public function __construct($pageTitle, $date, $path, $pageType = null)
  {
    $definition = '';
    if ($pageType === 'day') {
      $definition = Measures::$definitions[substr($path, 4)];
    }
    $this->definition = $definition;
    $formattedDate = Carbon::createFromFormat('Y-m-d', $date)->format('F j, Y');
    $this->pageTitle = $pageTitle;
    $this->pageSubtitle = $formattedDate;
    $this->pageType = $pageType;
  }

  /**
   * Get the view / contents that represent the component.
   *
   * @return \Illuminate\View\View|string
   */
  public function render()
  {
    return view('components.page-title');
  }
}
