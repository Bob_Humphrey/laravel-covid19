<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Helpers\Measures;
use App\Helpers\Scales;
use Carbon\Carbon;
use stdClass;

class StateDataTable extends Component
{
  public $dataTable;
  public $date;
  public $formattedDate;
  public $state;
  public $stateName;
  public $population;
  /**
   * Create a new component instance.
   *
   * @return void
   */
  public function __construct($record, $date, $state, $stateName, $population)
  {
    $dataTable = [];
    $scales = new Scales;
    foreach (Measures::$measures as $key => $value) {
      $measureAmount = $record[$key];
      $item = new stdClass();
      $item->color = $scales->getColor($record['category_' . $key]);
      $item->style = "background-color:" . $item->color . ";";
      $item->rank = $record['rank_' . $key];
      $item->name = $value;
      $item->measure = $key;
      $item->measureAmount = $measureAmount;
      $item->historyUrl = url('history/' . $key . '/' . $state);
      $item->url = url('day/' . $key . '/' . $date);
      $item->formattedMeasure = number_format($measureAmount);
      if (in_array($key, Measures::$percentage)) {
        $item->formattedMeasure .= '%';
      }
      $dataTable[] = $item;
    }
    $population->formattedPopulation = number_format($population->population);
    $this->dataTable = $dataTable;
    $this->date = $date;
    $this->formattedDate = Carbon::createFromFormat('Y-m-d', $date)
      ->format('F j, Y');
    $this->state = $state;
    $this->stateName = $stateName;
    $this->population = $population;
  }

  /**
   * Get the view / contents that represent the component.
   *
   * @return \Illuminate\View\View|string
   */
  public function render()
  {
    return view('components.state-data-table');
  }
}
