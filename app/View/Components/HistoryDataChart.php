<?php

namespace App\View\Components;

use Illuminate\View\Component;
use ArielMejiaDev\LarapexCharts\LarapexChart;
use Carbon\Carbon;
use App\Helpers\Measures;
use Illuminate\Support\Facades\Log;

class HistoryDataChart extends Component
{
  public $chart;
  /**
   * Create a new component instance.
   *
   * @return void
   */
  public function __construct($data, $measure = null, $date = null, $height = null)
  {

    $xAxis = [];
    $chartData = [];
    $earliestDate = Carbon::createFromFormat('Y-m-d', '2020-03-20');
    if (in_array($measure, Measures::$startChartApril15)) {
      $earliestDate = Carbon::createFromFormat('Y-m-d', '2020-04-15');
    }
    if (in_array($measure, Measures::$startChartMay1)) {
      $earliestDate = Carbon::createFromFormat('Y-m-d', '2020-05-01');
    }
    $latestDate = Carbon::today();
    if ($date !== null) {
      $latestDate = Carbon::createFromFormat('Y-m-d', $date);
    }

    foreach ($data as $item) {
      $dt = Carbon::createFromFormat('Y-m-d', $item->day);
      if (($dt->greaterThanOrEqualTo($earliestDate))
        && ($dt->lessThanOrEqualTo($latestDate))
      ) {
        $chartData[] = $item->measure;
        if ($dt->day === 1) {
          $xAxis[] = $dt->month . '/' . $dt->day;
        } else {
          $xAxis[] = '';
        }
      }
    }

    $chart = (new LarapexChart)->setType('area')
      ->setXAxis(array_reverse($xAxis))
      ->setDataset([
        [
          'name'  =>  '',
          'data'  =>  array_reverse($chartData)
        ]
      ]);

    if ($height) {
      $chart->setHeight(intval($height));
    }

    $this->chart = $chart;
  }


  /**
   * Get the view / contents that represent the component.
   *
   * @return \Illuminate\View\View|string
   */
  public function render()
  {
    return view('components.history-data-chart');
  }
}
