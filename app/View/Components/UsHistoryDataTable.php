<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Helpers\Measures;
use Carbon\Carbon;


class UsHistoryDataTable extends Component
{
  public $dataTable;
  public $measure;
  public $measureName;

  /**
   * Create a new component instance.
   *
   * @return void
   */
  public function __construct($data, $measure, $measureName)
  {
    $dataTable = [];
    foreach ($data as $item) {
      $item->date = Carbon::createFromFormat('Y-m-d', $item->day);
      $item->formattedDate = $item->date->format('F j, Y');
      $item->url = url('day/' . $measure . '/' . $item->day);
      $item->usUrl = url('us/' . $item->day);
      $item->formattedMeasure = number_format($item->measure);
      if (in_array($measure, Measures::$percentage)) {
        $item->formattedMeasure .= '%';
      }
      $dataTable[] = $item;
    }
    $this->dataTable = $dataTable;
    $this->measure = $measure;
    $this->measureName = $measureName;
  }

  /**
   * Get the view / contents that represent the component.
   *
   * @return \Illuminate\View\View|string
   */
  public function render()
  {
    return view('components.us-history-data-table');
  }
}
