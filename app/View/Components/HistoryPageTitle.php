<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Helpers\Measures;

class HistoryPageTitle extends Component
{
  public $pageTitle;
  public $pageSubtitle;
  public $measure;
  public $definition;
  public $pageType;

  /**
   * Create a new component instance.
   *
   * @return void
   */
  public function __construct($pageTitle, $stateName, $measure, $pageType = null)
  {
    $definition = '';
    if ($pageType === 'history') {
      $definition = Measures::$definitions[$measure];
    }
    $this->pageTitle = $pageTitle;
    $this->pageSubtitle = $stateName;
    $this->definition = $definition;
    $this->pageType = $pageType;
  }

  /**
   * Get the view / contents that represent the component.
   *
   * @return \Illuminate\View\View|string
   */
  public function render()
  {
    return view('components.page-title');
  }
}
