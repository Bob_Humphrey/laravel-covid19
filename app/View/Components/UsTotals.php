<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\Support\Facades\Log;

class UsTotals extends Component
{
  public $usData;

  /**
   * Create a new component instance.
   *
   * @return void
   */
  public function __construct($usData)
  {
    $this->usData = $usData;
  }

  /**
   * Get the view / contents that represent the component.
   *
   * @return \Illuminate\View\View|string
   */
  public function render()
  {
    return view('components.us-totals');
  }
}
