<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class SimplePageTitle extends Component

{
  public $pageTitle;
  public $pageSubtitle;
  public $definition;
  /**
   * Create a new component instance.
   *
   * @return void
   */
  public function __construct($pageTitle, $date)
  {
    $formattedDate = Carbon::createFromFormat('Y-m-d', $date)->format('F j, Y');
    $this->pageTitle = $pageTitle;
    $this->pageSubtitle = $formattedDate;
    $this->definition = '';
  }

  /**
   * Get the view / contents that represent the component.
   *
   * @return \Illuminate\View\View|string
   */
  public function render()
  {
    return view('components.simple-page-title');
  }
}
