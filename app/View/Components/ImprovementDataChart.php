<?php

namespace App\View\Components;

use Illuminate\View\Component;
use ArielMejiaDev\LarapexCharts\LarapexChart;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class ImprovementDataChart extends Component
{
  public $chart;
  /**
   * Create a new component instance.
   *
   * @return void
   */
  public function __construct($dataWorse, $dataSame, $dataBetter, $date)
  {

    //Log::info($date);

    $xAxis = [];
    $earliestDate = Carbon::createFromFormat('Y-m-d', '2020-04-01');
    $latestDate = Carbon::today();
    if ($date !== null) {
      $latestDate = Carbon::createFromFormat('Y-m-d', $date);
    }

    //Log::info($latestDate->format('Y-m-d'));

    foreach ($dataWorse as $item) {
      $dt = Carbon::createFromFormat('Y-m-d', $item->day);
      if (($dt->greaterThanOrEqualTo($earliestDate))
        && ($dt->lessThanOrEqualTo($latestDate))
      ) {
        $worseData[] = $item->measure;
        if ($dt->day === 1) {
          $xAxis[] = $dt->month . '/' . $dt->day;
        } else {
          $xAxis[] = '';
        }
      }
    }

    foreach ($dataSame as $item) {
      $dt = Carbon::createFromFormat('Y-m-d', $item->day);
      if (($dt->greaterThanOrEqualTo($earliestDate))
        && ($dt->lessThanOrEqualTo($latestDate))
      ) {
        $sameData[] = $item->measure;
      }
    }
    foreach ($dataBetter as $item) {
      $dt = Carbon::createFromFormat('Y-m-d', $item->day);
      if (($dt->greaterThanOrEqualTo($earliestDate))
        && ($dt->lessThanOrEqualTo($latestDate))
      ) {
        $betterData[] = $item->measure;
      }
    }

    $chart = (new LarapexChart)->setType('line')
      ->setColors(['#e53935', '#FDD835', '#7CB342'])
      ->setXAxis(array_reverse($xAxis))
      ->setDataset([
        [
          'name'  =>  'States Getting Worse',
          'data'  =>  array_reverse($worseData)
        ],
        [
          'name'  =>  'States Staying the Same',
          'data'  =>  array_reverse($sameData)
        ],
        [
          'name'  =>  'States Getting Better',
          'data'  =>  array_reverse($betterData)
        ]
      ]);

    $this->chart = $chart;
  }


  /**
   * Get the view / contents that represent the component.
   *
   * @return \Illuminate\View\View|string
   */
  public function render()
  {
    return view('components.improvement-data-chart');
  }
}
