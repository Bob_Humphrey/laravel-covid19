<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Helpers\States;
use App\Helpers\Scales;
use App\Helpers\Measures;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class DayDataTable extends Component
{
  public $stateData;
  public $usData;
  public $dataTable;
  public $path;
  public $date;
  public $formattedDate;
  public $measureName;
  public $usUrl;
  public $usHistoryUrl;
  /**
   * Create a new component instance.
   *
   * @return void
   */
  public function __construct($stateData, $usData, $path, $date, $measureName)
  {
    $scales = new Scales();
    $pathArray = explode("/", $path);
    $page = $pathArray[1] === "" ? "total_deaths" : $pathArray[1];
    $dataTable = [];
    for ($i = 0; $i < count($stateData); $i++) {
      $s = $stateData[$i];
      $s->name = States::getStateName($s->state);
      $s->nameShort = substr($s->name, 0, 10);
      $s->color = $scales->getColor($s->category);
      $s->style = "background-color:" . $s->color . ";";
      $s->historyUrl = url('history/' . $page . '/' . $s->state);
      $s->stateUrl = url('state/' . $s->state . '/' . $date);
      $s->formattedMeasure = $s->measure == 9999999 ? "" : number_format($s->measure);
      if (in_array($page, Measures::$percentage)) {
        $s->formattedMeasure .= '%';
      }
      $dataTable[] = $s;
    }
    $usData->formattedMeasure = number_format($usData->measure);
    if (in_array($page, Measures::$percentage)) {
      $usData->formattedMeasure .= '%';
    }

    $this->path = $path;
    $this->date = $date;
    $this->formattedDate = Carbon::createFromFormat('Y-m-d', $date)
      ->format('F j, Y');
    $this->measureName = $measureName;
    $this->dataTable = $dataTable;
    $this->usData = $usData;
    $this->usUrl = url('us/' . $date);
    $this->usHistoryUrl = url('us/history/' . $page);
  }

  /**
   * Get the view / contents that represent the component.
   *
   * @return \Illuminate\View\View|string
   */
  public function render()
  {
    return view('components.day-data-table');
  }
}
