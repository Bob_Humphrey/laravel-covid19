<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StateHistoricalData extends Model
{
  protected $table = 'state_historical_data';
}
